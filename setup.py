import os
import sys

from cx_Freeze import setup, Executable

from src.pysheet.util.miscellaneous import get_assets_path

includefiles = [os.path.join(os.path.dirname(__file__), "src", "pysheet", "ui", "assets")]
includes = []
excludes = ['tkinter']
packages = ['coloredlogs', 'xlrd', 'xlwt', 'psutil']

base = None
if (sys.platform == "win32"):
    base = "Win32GUI"

setup(
    name="Pysheet",
    version="1.0",
    author="StonedMoose",
    description="A PyQt program to compare excel and csv files",
    options={
        'build_exe': {
            'includes': includes,
            'excludes': excludes,
            'packages': packages,
            'include_files': includefiles,
            'include_msvcr': True,
            'add_to_path': True
        }
    },
    executables=[Executable("src/pysheet/app.py", base=base, targetName="pysheet.exe")]
)
