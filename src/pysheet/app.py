#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication

from src.pysheet.ui.main_window import MainWindow
from src.pysheet.util.miscellaneous import ScreenType, get_assets_path


def parse_command_line(qApplication: QApplication):
    """
    Parse command line arguments
    :return: the args object containing user's arguments
    """
    parser = argparse.ArgumentParser(description='Launch Pysheet a python program to do cross-searches in excel and csv files')
    parser.add_argument('-l', '--log', type=str,
                        help='Choose log level between {} {} {} {} {}'.format('DEBUG', 'INFO', 'WARN',
                                                                              'ERROR', 'CRITICAL'),
                        default='INFO')
    parser.add_argument('-s', '--screen', type=ScreenType(qApplication), default=0,
                        help='Choose screen to launch the app on')
    return parser.parse_args()


def configure_logger(args):
    os.environ['LOG_LEVEL'] = args.log
    os.environ['COLOREDLOGS_LOG_FORMAT'] = '%(asctime)s [%(name)s|%(process)d] ' \
                                           '%(levelname)s:  %(message)s '
    os.environ['COLOREDLOGS_DATE_FORMAT'] = '%H:%M:%S'
    os.environ['COLOREDLOGS_LEVEL_STYLES'] = 'spam=22;debug=224;verbose=34;info=cyan;warning=202;success=118,' \
                                             'bold;error=124;critical=background=red'


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    args = parse_command_line(app)
    configure_logger(args)

    main_window = MainWindow()
    screens = app.screens()
    main_window.move(screens[args.screen].geometry().left(), screens[args.screen].geometry().top())
    main_window.showMaximized()

    app.setWindowIcon(QtGui.QIcon(os.path.join(get_assets_path(), "app_icon.png")))

    sys.exit(app.exec_())
