import os
import re
from time import strftime, gmtime

from PyQt5 import QtWidgets, QtCore, QtGui

from src.pysheet.util.miscellaneous import get_assets_path


class ComputeDialog(QtWidgets.QDialog):

    def __init__(self, total_steps, parent=None):
        super().__init__(parent)
        self.__init_ui()
        self.progress_value = 0
        self.steps = total_steps
        self.file_steps = 0
        self.parent().tick.connect(self.update_progress_bar)
        self.parent().ended.connect(self.complete_progress_bar)

    def __init_ui(self):

        self.setWindowTitle("Compute window")

        self.setStyleSheet("background: #282a36;\n"
                           "background-color: #303030;\n"
                           "color: #ebebeb;\n")

        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setMinimumWidth(400)
        self.layout = QtWidgets.QVBoxLayout()

        self.error_widget = QtWidgets.QWidget()
        self.error_layout = QtWidgets.QVBoxLayout()

        self.folder_error_label = QtWidgets.QLabel("- Invalid folder")
        self.filename_error_label = QtWidgets.QLabel("- Invalid filename")
        self.format_error_label = QtWidgets.QLabel("- You must choose between csv or excel format")

        for l in [self.folder_error_label, self.filename_error_label, self.format_error_label]:
            l.hide()
            self.error_layout.addWidget(l)

        self.error_widget.setLayout(self.error_layout)
        self.error_widget.hide()
        self.error_widget.setStyleSheet("QWidget {border: 1px solid red; border-radius: 5px; background-color: #e06974;}"
                                        "QLabel { border: none}")
        self.layout.addWidget(self.error_widget)

        self.path_layout = QtWidgets.QHBoxLayout()

        path_label = QtWidgets.QLabel("Output Folder: ")
        self.path_layout.addWidget(path_label)

        self.path = QtWidgets.QLineEdit()
        self.path.setText(self.parent().path)
        self.path_layout.addWidget(self.path)
        self.layout.addLayout(self.path_layout)

        self.filename_layout = QtWidgets.QHBoxLayout()

        self.filename_layout.addWidget(QtWidgets.QLabel("Filename:           "))
        self.filename = QtWidgets.QLineEdit()
        self.filename.setText("export_{}".format(strftime("%Y-%m-%d_%Hh%M", gmtime())))
        self.filename_layout.addWidget(self.filename)

        self.layout.addLayout(self.filename_layout)

        self.folder_icon = QtGui.QIcon(os.path.join(get_assets_path(), "folder.svg"))
        self.folder_btn = QtWidgets.QToolButton()
        self.folder_btn.setIcon(self.folder_icon)
        self.folder_btn.clicked.connect(self.open_file_dialog)
        self.path_layout.addWidget(self.folder_btn)

        self.label = QtWidgets.QLabel("Choose the export format:")
        self.layout.addWidget(self.label)

        self.csv_radio_btn = QtWidgets.QRadioButton("csv")
        self.csv_radio_btn.setChecked(True)
        self.layout.addWidget(self.csv_radio_btn)

        self.excel_radio_btn = QtWidgets.QRadioButton("xls")
        self.layout.addWidget(self.excel_radio_btn)

        self.widget = QtWidgets.QWidget()
        self.widget.setLayout(self.layout)

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addWidget(self.widget)

        self.button = QtWidgets.QPushButton("Let's go")
        self.button.clicked.connect(self.do_compute)
        main_layout.addWidget(self.button)

        self.progress_label = QtWidgets.QLabel("Searching matching result:")
        self.progress_label.hide()
        main_layout.addWidget(self.progress_label)

        self.progress = QtWidgets.QProgressBar()
        self.progress.hide()
        main_layout.addWidget(self.progress)

        self.progress_file_label = QtWidgets.QLabel("Writing result:")
        self.progress_file_label.hide()
        main_layout.addWidget(self.progress_file_label)

        self.progress_file = QtWidgets.QProgressBar()
        self.progress_file.hide()
        main_layout.addWidget(self.progress_file)

        self.no_result_label = QtWidgets.QLabel("No row matching !")
        self.no_result_label.setStyleSheet("color: red")
        self.no_result_label.hide()
        main_layout.addWidget(self.no_result_label)

        main_layout.addStretch()

        self.ok_btn = QtWidgets.QPushButton("OK")
        self.ok_btn.hide()
        self.ok_btn.clicked.connect(self.close)
        main_layout.addWidget(self.ok_btn)

        self.setLayout(main_layout)

    def do_compute(self):
        if os.path.isdir(self.path.text()) and re.fullmatch(r'[0-9A-Za-z-_ ]*', self.filename.text()) \
                and (self.csv_radio_btn.isChecked() or self.excel_radio_btn.isChecked()):
            self.hide_errors()
            self.button.hide()
            self.widget.hide()
            self.progress.show()
            self.progress_label.show()
            csv_or_excel = "csv" if self.csv_radio_btn.isChecked() else "xls"
            self.parent().compute(self.path.text(), self.filename.text(), csv_or_excel)

        else:
            self.display_error()

    def display_error(self):
        self.error_widget.show()
        if not os.path.isdir(self.path.text()):
            self.folder_error_label.show()
        if not re.fullmatch(r'[0-9A-Za-z-_ ]*', self.filename.text()):
            self.filename_error_label.show()
        if not self.csv_radio_btn.isChecked() and not self.excel_radio_btn.isChecked():
            self.format_error_label.show()

    def hide_errors(self):
        self.error_widget.hide()
        for l in [self.folder_error_label, self.filename_error_label, self.format_error_label]:
            l.hide()

    def open_file_dialog(self):
        directory_name = QtWidgets.QFileDialog.getExistingDirectory()
        if directory_name:
            self.path.setText(directory_name)

    def update_progress_bar(self):
        print(self.progress.value())
        if self.progress.value() < 100:
            self.progress_value += 1
            self.progress.setValue(int(100 * self.progress_value / self.steps))
        elif self.progress.value() and self.file_steps != 0:
            if self.progress_file_label.isHidden():
                self.progress_file_label.show()
                self.progress_file.show()
            self.progress_value += 1
            self.progress_file.setValue(int(100 * self.progress_value / self.file_steps))

    def complete_progress_bar(self):
        if self.progress_file.value() == -1:
            self.progress.setValue(100)
            self.progress_value = 0
        else:
            self.progress_file.setValue(100)
            self.ok_btn.show()

    def show_no_result_label(self):
        self.no_result_label.show()
        self.ok_btn.show()

