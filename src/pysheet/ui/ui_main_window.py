#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'creator/mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!
import os

from PyQt5 import QtCore, QtGui, QtWidgets

from src.pysheet.util.miscellaneous import get_assets_path


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setStyleSheet("QPushButton {\n"
                                 "    padding: 3px;\n"
                                 "}\n"
                                 "QListView::item:selected {background-color: rgba(63, 68, 87, 0.4)}"
                                 "QListView::item:hover {background: rgba(63, 68, 87, 0.6);}\n"
                                 "QGroupBox {\n"
                                 "    background-color: #424242;\n"
                                 "    border-radius: 5px;\n"
                                 "}\n"
                                 "\n"
                                 "QScrollArea {\n"
                                 "    margin: 0px 5px 0px 5px;\n"
                                 "}\n"
                                 "QListWidgetItem:hover {background-color: red}\n"
                                 )
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("background: #282a36;\n"
                                         "background-color: #303030;\n"
                                         "color: #ebebeb;\n"
                                         "\n"
                                         "")
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.verticalLayoutForTree = QtWidgets.QVBoxLayout()
        self.file_model = QtWidgets.QFileSystemModel()

        self.tree_view = QtWidgets.QTreeView()
        self.tree_view.setModel(self.file_model)
        self.tree_view.hideColumn(2)
        self.tree_view.hideColumn(3)
        self.tree_view.setStyleSheet("QTreeView {background-color: #424242;margin-left: 5px;"
                                     "selection-background-color: #ffa726;}\n")
        self.tree_view.header().setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)

        self.open_file_btn = QtWidgets.QPushButton(self.centralwidget)
        self.open_file_btn.setStyleSheet("background-color: #673ab7;\n"
                                         "margin-left: 5px;\n"
                                         "margin-top: 3px;")
        self.open_file_btn.setObjectName("open_file_btn")

        self.verticalLayoutForTree.addWidget(self.open_file_btn)
        self.verticalLayoutForTree.addWidget(self.tree_view)
        self.horizontalLayout.addLayout(self.verticalLayoutForTree, 2)

        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setStyleSheet("background-color: #424242;\n"
                                      "margin: 3px 5px 0 5px;")
        self.listWidget.setObjectName("listWidget")
        self.horizontalLayout.addWidget(self.listWidget, 4)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)

        # layout for group boxes
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")

        # Group box 1
        self.groupbox_1 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupbox_1.setStyleSheet("background-color: #424242;\n"
                                      "margin: 0;\n"
                                      "margin-left: 5px;")
        self.groupbox_1.setObjectName("groupbox_1")
        self.groupbox_1_layout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_5.addWidget(self.groupbox_1)

        self.combobox = QtWidgets.QComboBox(self.groupbox_1)
        self.combobox.setObjectName("comboBox")
        self.groupbox_1_layout.addWidget(self.combobox)

        self.combobox_layout = QtWidgets.QVBoxLayout()
        self.combobox_layout_item = QtWidgets.QWidget()
        self.combobox_layout_item.setLayout(self.combobox_layout)
        self.groupbox_1_layout.addWidget(self.combobox_layout_item)
        self.groupbox_1.setLayout(self.groupbox_1_layout)

        # Group box 2
        self.groupbox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupbox_2.setStyleSheet("background-color: #424242;\n"
                                      "margin: 0;")
        self.groupbox_2.setObjectName("group_box_2")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.groupbox_2)
        self.verticalLayout_9.setObjectName("verticalLayout_9")

        self.inRadioButton = QtWidgets.QRadioButton("IN", self.groupbox_2)
        self.inRadioButton.setObjectName("inRadioButton")
        self.verticalLayout_9.addWidget(self.inRadioButton)

        self.notInRadioButton = QtWidgets.QRadioButton("NOT IN", self.groupbox_2)
        self.notInRadioButton.setObjectName("notInRadioButton")
        self.verticalLayout_9.addWidget(self.notInRadioButton)

        self.equalRadioButton = QtWidgets.QRadioButton("EQUAL", self.groupbox_2)
        self.equalRadioButton.setObjectName("equalRadioButton")
        self.equalRadioButton.hide()
        self.verticalLayout_9.addWidget(self.equalRadioButton)

        self.notEqualRadioButton = QtWidgets.QRadioButton("NOT EQUAL", self.groupbox_2)
        self.notEqualRadioButton.setObjectName("notEqualRadioButton")
        self.notEqualRadioButton.hide()
        self.verticalLayout_9.addWidget(self.notEqualRadioButton)

        self.horizontalLayout_5.addWidget(self.groupbox_2)

        # Group box 3
        self.groupbox_3 = QtWidgets.QWidget()
        self.groupbox_3.setStyleSheet("background-color: #424242;\n"
                                      "margin: 0;\n"
                                      "margin-left: 5px;")
        self.groupbox_3.setObjectName("groupbox_3")
        self.groupbox_3_layout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_5.addWidget(self.groupbox_3)

        self.combobox_2 = QtWidgets.QComboBox(self.groupbox_3)
        self.combobox_2.setObjectName("comboBox2")
        self.groupbox_3_layout.addWidget(self.combobox_2)

        self.combobox_2_layout = QtWidgets.QVBoxLayout()
        self.combobox_2_layout_item = QtWidgets.QWidget()
        self.combobox_2_layout_item.setLayout(self.combobox_2_layout)
        self.groupbox_3_layout.addWidget(self.combobox_2_layout_item)
        self.groupbox_3.setLayout(self.groupbox_3_layout)

        # Group box 4
        self.group_box_4 = QtWidgets.QGroupBox(self.centralwidget)
        self.group_box_4.setStyleSheet("background-color: #424242;\n"
                                       "margin: 0;\n"
                                       "")
        self.group_box_4.setObjectName("group_box_4")

        self.vboxLayout = QtWidgets.QVBoxLayout(self.group_box_4)

        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")

        self.addButton = QtWidgets.QPushButton("ADD", self.group_box_4)
        self.addButton.setStyleSheet("")
        self.addButton.setObjectName("addButton")
        self.addButton.setEnabled(False)

        self.horizontalLayout_4.addWidget(self.addButton)

        self.andButton = QtWidgets.QPushButton("AND", self.group_box_4)
        self.andButton.setObjectName("andButton")
        self.horizontalLayout_4.addWidget(self.andButton)
        self.andButton.hide()

        self.revertButton = QtWidgets.QToolButton(self.group_box_4)
        self.revertButton.setIcon(QtGui.QIcon(os.path.join(get_assets_path(), "revert.png")))
        self.revertButton.setObjectName("revertButton")
        self.horizontalLayout_4.addWidget(self.revertButton)
        self.revertButton.hide()

        self.orButton = QtWidgets.QPushButton("OR", self.group_box_4)
        self.orButton.setObjectName("orButton")
        self.horizontalLayout_4.addWidget(self.orButton)
        self.orButton.hide()
        self.horizontalLayout_5.addWidget(self.group_box_4)

        self.clear_btn = QtWidgets.QPushButton("Clear")
        self.clear_btn.hide()
        self.vboxLayout.addWidget(self.clear_btn)
        self.vboxLayout.addStretch()
        self.vboxLayout.addLayout(self.horizontalLayout_4)
        self.vboxLayout.addStretch()

        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.command_edit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.command_edit.setEnabled(False)
        self.command_edit.setStyleSheet("background-color: #424242;\n"
                                        "margin-right: 5px;\n"
                                        "max-width: 400px;\n"
                                        "")
        self.command_edit.setObjectName("command_edit")
        self.verticalLayout_5.addWidget(self.command_edit)
        self.horizontalLayout_5.addLayout(self.verticalLayout_5)
        self.gridLayout.addLayout(self.horizontalLayout_5, 2, 0, 1, 1)
        self.compute_btn = QtWidgets.QPushButton(self.centralwidget)
        self.compute_btn.setStyleSheet("QPushButton {background-color: #673ab7;\n"
                                       "margin: 0 5px 10px 5px;}"
                                       "QPushButton:disabled {background-color: rgba(105,89,134, 0.5); border: 1px solid; color: white}")
        self.compute_btn.setEnabled(False)
        self.compute_btn.setObjectName("compute_btn")

        self.gridLayout.addWidget(self.compute_btn, 3, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Pysheet"))
        self.compute_btn.setText(_translate("MainWindow", "Compute"))
        self.open_file_btn.setText(_translate("MainWindow", "Open directory"))
