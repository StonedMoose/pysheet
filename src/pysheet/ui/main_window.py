#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import os
import re
from typing import Dict, Tuple

import psutil
import xlrd
import xlwt
from PyQt5 import QtCore, Qt
from PyQt5.QtCore import QThreadPool, QTimer, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QFileDialog, QMainWindow, QListWidgetItem, QWidget, QLabel, QHBoxLayout, QMessageBox, \
    QListView, \
    QComboBox, QVBoxLayout, QRadioButton, QProgressBar, QGroupBox, QLayout

from src.pysheet.ui.ComputeDialog import ComputeDialog
from src.pysheet.ui.ui_main_window import Ui_MainWindow
from src.pysheet.util.Query import Query
from src.pysheet.util.Statement import Statement
from src.pysheet.util.Workbook import Workbook
from src.pysheet.util.Worker import Worker
from src.pysheet.util.miscellaneous import get_logger, get_assets_path


class MainWindow(QMainWindow, Ui_MainWindow):

    opened_files: Dict[str, Workbook] = {}
    radio_btns = {}
    query: Query = Query()
    queue_limit = 2
    worker_size = 0
    worker_queue = []
    path = None
    output_path = ""
    tick = pyqtSignal()
    ended = pyqtSignal()

    def __init__(self):
        """
        Constructor
        """
        QMainWindow.__init__(self)
        Ui_MainWindow.setupUi(self, self)
        self.__init_model()
        self.__bind_actions()
        self.__my_setup_ui()
        self.__init_update_ui_task()

    def __init_model(self):
        """
        Init model values
        """
        self.process = psutil.Process(os.getpid())
        self.mem = psutil.virtual_memory()
        self.thread_pool = QThreadPool()
        self.log = get_logger(__name__)
        self.radio_btns[self.combobox.objectName()] = []
        self.radio_btns[self.combobox_2.objectName()] = []

        self.query.signals.result.connect(self.handle_query_result)
        self.query.signals.progress.connect(self.update_compute_progress_bar)

        self.notification = Qt.QSystemTrayIcon(Qt.QIcon(os.path.join(get_assets_path(), "app_icon.png")))
        self.notification.show()

    def __init_update_ui_task(self):
        self.timer = QTimer()
        self.timer.setInterval(5000)
        self.timer.timeout.connect(self.__update_ui)
        self.timer.start()

    def __update_ui(self):
        self.set_progress_bar_value()

    def __my_setup_ui(self):
        """
        Custom setup_ui
        """
        # Error msg box
        self.error_msg_box = QMessageBox(self)
        self.error_msg_box.setModal(True)
        # Info msg box
        self.info_msg_box = QMessageBox(self)
        self.info_msg_box.setModal(True)

        # status bar
        self.memory_usage_widget = QProgressBar(self.statusbar)
        self.memory_usage_widget.setFixedWidth(150)
        self.memory_usage_widget.setStyleSheet("QProgressBar {height: 15px; border: 1px solid black} "
                                               "QProgressBar::chunk {background-color: #ffa726;}")
        self.statusbar.addWidget(self.memory_usage_widget)
        self.memory_label = QLabel()
        self.statusbar.addWidget(self.memory_label, 2)
        self.file_queue_label = QLabel("")
        self.statusbar.addWidget(self.file_queue_label)

    def __bind_actions(self):
        """
        Bind actions on item ui
        """
        self.open_file_btn.clicked.connect(self.show_file_dialog)
        self.addButton.clicked.connect(self.init_query)
        self.andButton.clicked.connect(self.add_and_statement)
        self.orButton.clicked.connect(self.add_or_statement)
        self.revertButton.clicked.connect(self.revert_query)
        self.clear_btn.clicked.connect(self.clear_query)
        self.compute_btn.clicked.connect(self.show_compute_window)

    def show_file_dialog(self):
        """
        Show file dialog to pick a file from File System
        """
        directory_name = QFileDialog.getExistingDirectory()
        if directory_name:
            self.path = directory_name
            self.file_model.setRootPath(directory_name)
            self.tree_view.setRootIndex(self.file_model.index(directory_name))

            self.file_model.setNameFilterDisables(False)
            self.file_model.setNameFilters(["*.csv", "*.xls", "*.xlsx"])
            self.tree_view.doubleClicked.connect(self.open_file)

    def open_file(self, index: QtCore.QModelIndex):
        file_name = self.file_model.filePath(index)
        if file_name:
            if file_name in self.opened_files:
                self.show_info_box("The file you try to open is already in list")
            elif re.search("\.csv$", file_name):
                self.open_file_wrapper(file_name, "csv")

            elif re.search("\.xlsx?", file_name):
                self.open_file_wrapper(file_name, "xls")
            else:
                self.show_error_box("Cannot open file: " + file_name, detailed_text="You must choose excel or csv files. If "
                                                                                        "you provided this kind, they may be "
                                                                                        "corrupted.")

    def show_error_box(self, text, title="Error", detailed_text=""):
        """
        show error dialog with given message
        """
        self.error_msg_box.setIcon(QMessageBox.Warning)
        self.error_msg_box.setWindowTitle(title)
        self.error_msg_box.setText(text)
        self.error_msg_box.setDetailedText(detailed_text)
        self.error_msg_box.show()

    def show_info_box(self, text, title="Information", detailed_text=""):
        """
        show information dialog with given message
        """
        self.info_msg_box.setIcon(QMessageBox.Information)
        self.info_msg_box.setWindowTitle(title)
        self.info_msg_box.setText(text)
        self.info_msg_box.setDetailedText(detailed_text)
        self.info_msg_box.show()

    def add_item(self, rowName):
        """
        Add row matching a file being opened
        :param rowName: the file name
        """
        self.log.debug("add item")
        itemN = QListWidgetItem()
        # Create widget
        widget = QWidget()
        widget.setStyleSheet("background-color: transparent")
        widgetText = QLabel(rowName)

        label = QLabel(rowName)

        # item row layout
        widgetLayout = QHBoxLayout()
        widgetText.setStyleSheet("background-color: transparent;")
        widgetLayout.addWidget(widgetText)
        widgetLayout.addStretch()
        widgetLayout.addWidget(label)

        widget.setLayout(widgetLayout)
        itemN.setSizeHint(widget.sizeHint())

        # Add movie to Workbook
        wb = Workbook(name=rowName, item=itemN, layout=widgetLayout, label=label, main_window=self)
        wb.start_throbber()
        self.opened_files[rowName] = wb
        # Add widget to QListWidget
        self.listWidget.addItem(itemN)
        self.listWidget.setItemWidget(itemN, widget)

    def add_queued_item(self, rowName):
        """
        Add item in queue
        :param rowName: the file name
        """
        self.log.debug("add queued item")
        itemN = QListWidgetItem()
        # Create widget
        widget = QWidget()
        widget.setStyleSheet("background-color: transparent")
        widgetText = QLabel(rowName)

        label = QLabel(rowName)

        # item row layout
        widgetLayout = QHBoxLayout()
        widgetText.setStyleSheet("background-color: transparent;")
        widgetLayout.addWidget(widgetText)
        widgetLayout.addStretch()
        widgetLayout.addWidget(label)

        widget.setLayout(widgetLayout)
        itemN.setSizeHint(widget.sizeHint())

        # Add movie to Workbook
        wb = Workbook(name=rowName, item=itemN, layout=widgetLayout, label=label, main_window=self)
        wb.set_queue_icon()
        self.opened_files[rowName] = wb
        # Add widget to QListWidget
        self.listWidget.addItem(itemN)
        self.listWidget.setItemWidget(itemN, widget)

    def add_file_to_combos(self, filename):
        """
        Add file to combo boxes
        :param filename: The file name to add
        """
        for comboGroup in [(self.groupbox_1, self.combobox, self.combobox_layout), (self.groupbox_3, self.combobox_2, self.combobox_2_layout)]:
            listView = QListView()
            listView.setStyleSheet("QListView::item:hover {background: rgba(63, 68, 87, 0.6)}")
            comboGroup[1].setView(listView)
            comboGroup[1].addItem(os.path.basename(filename))
            self.refresh_column_radio_btns(comboGroup)

        self.update_add_btn_status()

        self.combobox.currentIndexChanged.connect(lambda: self.refresh_column_radio_btns((self.groupbox_1, self.combobox, self.combobox_layout)))
        self.combobox_2.currentIndexChanged.connect(lambda: self.refresh_column_radio_btns((self.groupbox_3, self.combobox_2,
                                                                                            self.combobox_2_layout)))

    def handle_worker_result(self, result: Tuple[str, any]):
        """
        Slot triggered when worker finish opening a file
        :param result: the result AbstractSpreadsheet
        """
        self.worker_size -= 1
        self.update_queue_info()
        name, wb = result
        self.log.debug("Get worker result")
        self.opened_files[name].set_workbook(wb)
        self.opened_files[name].stop_throbber()

        if not self.opened_files[name].is_headers_valid():
            self.show_error_box("Cannot handle file {}, the headers are invalid".format(name),
                                detailed_text="To handle a excel file the headers in each sheets must be identical")
            self.remove_item_from_ui(self.opened_files[name])
            self.notification.showMessage("Invalid file", name)
            return

        self.notification.showMessage("Finish opening file", name)
        self.add_file_to_combos(name)
        self.launch_from_queue()

    def launch_from_queue(self):
        if self.worker_queue:
            self.worker_size += 1
            worker = self.worker_queue.pop(0)
            for key in self.opened_files:
                if self.opened_files[key].name == worker.args[0]:
                    self.opened_files[key].start_throbber()
            self.update_queue_info()
            self.thread_pool.start(worker)

    def open_file_wrapper(self, filename, file_type):
        """
        Wrap open file action
        :param filename:
        :param file_type:
        """
        if file_type == "xls":
            worker = Worker(self.open_xls_file, filename)
        elif file_type == "csv":
            worker = Worker(self.open_csv_file, filename)

        worker.signals.result.connect(self.handle_worker_result)

        if self.worker_size >= self.queue_limit:
            self.worker_queue.append(worker)
            self.add_queued_item(filename)
        else:
            self.worker_size += 1
            self.add_item(filename)
            self.thread_pool.start(worker)

        self.update_queue_info()

    def open_csv_file(self, filename):
        """
        Callback used by worker to open csv files
        :param filename: the file to open
        """
        with open(filename, 'r') as qbFile:
            return filename, list(csv.reader(qbFile, delimiter=";"))

    def open_xls_file(self, filename):
        """
        Callback used by worker to open an excel file
        :param filename: the file to open
        """
        with xlrd.open_workbook(filename, on_demand=True) as wb:
            return filename, wb

    def remove_item_from_ui(self, wb: Workbook):
        """
        Remove items from ui when delete file button is clicked.
        Remove entry in combo boxes and remove line in file list widget
        :param wb:
        """
        self.log.debug("Remove item from ui")
        self.listWidget.takeItem(self.listWidget.row(wb.item))
        self.opened_files.pop(wb.name)

        self.update_add_btn_status()

        for combo in [self.combobox, self.combobox_2]:
            combo.removeItem(combo.findText(os.path.basename(wb.name)))

        for statement in self.query:
            if os.path.basename(wb.name) in str(statement):
                self.clear_query()

    def refresh_column_radio_btns(self, combo_group: Tuple[QGroupBox, QComboBox, QVBoxLayout]):
        """
        Refresh radio button upon file selection in combo box
        :param combo_group: The reference to the combo box changed and the layout to refresh
        """
        self.log.debug("Refresh column radio btns for {}".format(combo_group[0].objectName()))
        groupbox = combo_group[0]
        combo = combo_group[1]
        layout = combo_group[2]
        for i in reversed(range(layout.count())):
            layout.takeAt(i).widget().deleteLater()

        for key in self.opened_files:
            wb = self.opened_files[key]
            if wb.name.endswith(combo.currentText()):
                for col in wb.columns:
                    radio_btn = QRadioButton(col, groupbox)
                    layout.addWidget(radio_btn)

        if combo.objectName() == "comboBox2" and \
                self.query.is_last_workbook2(combo.currentText()):
            self.show_equal_not_equal()
            self.hide_in_not_in()
        else:
            self.hide_equal_not_equal()
            self.show_in_not_in()

    def set_progress_bar_value(self):
        """
        Update progress bar displaying current memory usage
        """
        self.memory_usage_widget.setValue(self.process.memory_info().rss / self.mem.total)
        tot_mem_str = str(round(self.process.memory_info().rss/(2**20), 1))
        self.memory_label.setText("Memory usage ({} Mo)".format(tot_mem_str))

    def init_query(self):
        """
        Init query with SELECT and add it to command edit
        """
        workbook1 = self.get_workbook_from_combo(self.combobox.currentText())
        workbook2 = self.get_workbook_from_combo(self.combobox_2.currentText())
        column1 = self.get_selected_column(self.combobox_layout)
        column2 = self.get_selected_column(self.combobox_2_layout)
        comparator = self.get_selected_column(self.verticalLayout_9)

        if self.validate_query(workbook1.name, workbook2.name, column1, column2, comparator):
            statement = Statement(workbook1, workbook2, column1, column2, comparator, file_from=workbook1)
            self.query.add_statement(statement)
            self.command_edit.appendPlainText(str(statement))
            self.update_ui_when_query_not_empty()
            self.hide_in_not_in()
            self.show_equal_not_equal()

    def add_and_statement(self):
        """
        Build AND statement and add it to command edit
        """
        workbook1 = self.get_workbook_from_combo(self.combobox.currentText())
        workbook2 = self.get_workbook_from_combo(self.combobox_2.currentText())
        column1 = self.get_selected_column(self.combobox_layout)
        column2 = self.get_selected_column(self.combobox_2_layout)
        comparator = self.get_selected_column(self.verticalLayout_9)

        if self.validate_query(workbook1.name, workbook2.name, column1, column2, comparator):
            if comparator == "NOT IN" or comparator == "IN":
                self.hide_in_not_in()
                self.show_equal_not_equal()

            statement = Statement(workbook1, workbook2, column1, column2, comparator, boolean_operator="AND")
            self.query.add_statement(statement)
            self.command_edit.appendPlainText(str(statement))

    def add_or_statement(self):
        """
        Build OR statement and add it to command edit
        """
        workbook1 = self.get_workbook_from_combo(self.combobox.currentText())
        workbook2 = self.get_workbook_from_combo(self.combobox_2.currentText())
        column1 = self.get_selected_column(self.combobox_layout)
        column2 = self.get_selected_column(self.combobox_2_layout)
        comparator = self.get_selected_column(self.verticalLayout_9)

        if self.validate_query(workbook1.name, workbook2.name, column1, column2, comparator):
            if comparator == "NOT IN" or comparator == "IN":
                self.hide_in_not_in()
                self.show_equal_not_equal()
            statement = Statement(workbook1, workbook2, column1, column2, comparator, boolean_operator="OR")
            self.query.add_statement(statement)
            self.command_edit.appendPlainText(str(statement))

    def validate_query(self, file1: str, file2: str, column1: str, column2: str, comparator: str):
        """
        Validate a query before appending it to query text edit
        :param file1: the first file
        :param file2: the second file
        :param column1: the first column
        :param column2: the second column
        :param comparator: the comparator
        :return: a boolean representing the validation status
        """
        is_validated = True
        error = ""

        if file1 == file2:
            error += "- You can't compare two files which are identical\n"
            is_validated = False
        if not column1:
            error += "- You must choose a column for file {}\n".format(file1)
            is_validated = False
        if not column2:
            error += "- You must choose a column for file {}\n".format(file2)
            is_validated = False
        if not comparator:
            error += "- You must choose a comparator"
            is_validated = False

        if not is_validated:
            self.show_error_box("Your query is not correct", detailed_text=error)

        return is_validated

    def revert_query(self):
        """
        Revert query one step backward
        """
        self.query.pop()
        self.command_edit.clear()
        self.command_edit.appendPlainText("\n".join([str(statement) for statement in self.query]))
        if self.query.is_empty():
            self.update_ui_when_query_empty()
            self.show_in_not_in()
            self.hide_equal_not_equal()

        if self.query.is_last_workbook2(self.combobox_2.currentText()):
            self.show_equal_not_equal()
            self.hide_in_not_in()
        else:
            self.hide_equal_not_equal()
            self.show_in_not_in()

    def clear_query(self):
        """
        Clear query ie. command edit and query array
        """
        self.query.clear()
        self.command_edit.clear()
        self.update_ui_when_query_empty()
        self.show_in_not_in()
        self.hide_equal_not_equal()

    def update_ui_when_query_empty(self):
        self.hide_or_and()
        self.combobox.setEnabled(True)
        self.compute_btn.setEnabled(False)
        self.clear_btn.hide()
        
    def update_ui_when_query_not_empty(self):
        self.show_or_and()
        self.clear_btn.show()
        self.combobox.setEnabled(False)
        self.compute_btn.setEnabled(True)

    def get_selected_column(self, layout: QLayout):
        """
        Get selected column by iterating over radio button in given layout
        :param layout: the layout to iter through
        """
        for i in range(layout.count()):
            if layout.itemAt(i).widget().isChecked():
                return layout.itemAt(i).widget().text()

    def get_workbook_from_combo(self, combo_value: str):
        """
        Get workbook which name end with combo_value
        :param combo_value: the value that the workbook's name should end with
        """
        for key in self.opened_files:
            if self.opened_files[key].name.endswith(combo_value):
                return self.opened_files[key]

    def update_add_btn_status(self):
        """
        Update add button status (enabled or not) by checking the number of opened files
        """
        if len(self.opened_files) >= 2:
            self.addButton.setEnabled(True)
        else:
            self.addButton.setEnabled(False)

    def update_queue_info(self):
        self.file_queue_label.setText("Queue: {}, Worker: {}/{}".format(len(self.worker_queue), self.worker_size,
                                                                        self.queue_limit))

    def show_or_and(self):
        """
        Show OR, AND and REVERT buttons and hide ADD button
        """
        self.addButton.hide()
        self.orButton.show()
        self.andButton.show()
        self.revertButton.show()

    def hide_or_and(self):
        """
        Hide OR, AND and REVERT buttons and show ADD button
        """
        self.addButton.show()
        self.orButton.hide()
        self.andButton.hide()
        self.revertButton.hide()
        
    def show_in_not_in(self):
        self.inRadioButton.show()
        self.notInRadioButton.show()

    def hide_in_not_in(self):
        self.inRadioButton.hide()
        self.notInRadioButton.hide()
        
    def show_equal_not_equal(self):
        self.equalRadioButton.show()
        self.notEqualRadioButton.show()
        
    def hide_equal_not_equal(self):
        self.equalRadioButton.hide()
        self.notEqualRadioButton.hide()

    def show_compute_window(self):
        self.compute_window = ComputeDialog(parent=self, total_steps=self.query.get_total_steps())
        self.compute_window.show()

    def compute(self, path, filename, csv_or_excel):
        self.output_path = os.path.join(path, filename + "." + csv_or_excel)
        worker = Worker(self.query.compute)
        self.thread_pool.start(worker)

    pyqtSlot()
    def handle_query_result(self, result):
        self.ended.emit()
        self.write_result(result)

    @pyqtSlot()
    def update_compute_progress_bar(self, ):
        self.tick.emit()

    def write_result(self, result):
        result_nb = sum([len(result[key]) for key in result])

        if result_nb == 0:
            self.compute_window.show_no_result_label()
            self.notification.showMessage('Finish searching matching rows', 'No matching rows')
            return

        self.compute_window.file_steps = result_nb

        if self.output_path.endswith(".csv"):
            self.write_csv_result(result, self.output_path)
        else:
            self.write_xls_result(result, self.output_path)
        self.notification.showMessage('Finish writing results', "{} matching rows".format(result_nb))

    def write_xls_result(self, results, output_path):

        res = xlwt.Workbook()

        for sheet_name in results:
            sheet = res.add_sheet(sheet_name)

            for i_col, cell in enumerate(self.query.get_headers()):
                sheet.write(0, i_col, cell)

            for i_row, row in enumerate(results[sheet_name]):
                self.tick.emit()
                for i_col, cell in enumerate(row):
                    sheet.write(i_row + 1, i_col, cell)

        res.save(output_path)
        self.ended.emit()

    def write_csv_result(self, results, output_path):

        with open(output_path, 'w', newline='') as f:
            writer = csv.writer(f, delimiter=";")
            writer.writerow(self.query.get_headers())
            for sheet in results:
                for row in results[sheet]:
                    self.tick.emit()
                    writer.writerow(row)

        self.ended.emit()

