#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import traceback

from PyQt5.QtCore import QRunnable, pyqtSlot

from src.pysheet.util.WorkerSignals import WorkerSignals
from src.pysheet.util.miscellaneous import get_logger


class Worker(QRunnable):
    """
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and 
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    """

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()
        self.log = get_logger(__name__)

    @pyqtSlot()
    def run(self):
        """
        Initialize the runner function with passed args, kwargs.
        """

        # Retrieve args/kwargs here; and fire processing using them
        try:
            self.log.debug("Start working")
            result = self.fn(*self.args, **self.kwargs)
        except:
            self.log.critical("Exception during worker execution")
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.log.debug("Work done working")
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.log.debug("Worker finished")
            self.signals.finished.emit()  #
