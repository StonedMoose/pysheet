#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

from PyQt5 import QtCore, QtGui
from PyQt5.QtGui import QMovie, QPixmap, QIcon
from PyQt5.QtWidgets import QLabel, QHBoxLayout, QPushButton, QListWidgetItem
from xlrd import Book

from src.pysheet.util.miscellaneous import get_logger, get_assets_path, get_alias


class Workbook:
    alias_count = 0

    def __init__(self, name: str = None, workbook=None, throbber: QMovie = None, label: QLabel = None,
                 layout: QHBoxLayout = None, main_window=None, item: QListWidgetItem = None):
        self.log = get_logger(__name__)
        self.name = name
        self.alias = 1
        self.__wb = workbook
        self.__main_window = main_window
        self.item = item
        self.__layout = layout
        self.__throbber = throbber
        self.__label = label
        self.columns = []
        self.alias = get_alias(self.alias_count)
        self.alias_count += 1

        self.__infos = {
            "sheets": 1,
            "rows": 0,
            "columns": 0,
        }

    def set_workbook(self, workbook):
        """
        Set the workbook attribute
        :param workbook: the workbook either xlrd.Book or csv.Reader
        """
        self.log.debug("Set workbook " + str(type(workbook)))
        self.__wb = workbook
        self.__set_workbook_info()

    def is_workbook_set(self):
        return not not self.__wb

    def set_queue_icon(self):
        size = 40
        img = os.path.join(get_assets_path(), "queue.svg")
        icon = QIcon(img)
        self.__label.setPixmap(icon.pixmap(QtCore.QSize(size, size)))
        self.__label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

    def start_throbber(self):
        # crée un movie avec le throbber sous forme d'image "gif" animée
        imagif = os.path.join(get_assets_path(), "throbber.gif")

        # QLabel and QMovie for loading indicator
        size = 18
        self.__throbber = QMovie(imagif)
        self.__throbber.setScaledSize(QtCore.QSize(size, size))
        self.__label.resize(QtCore.QSize(size + 30, size + 2))
        # Prevent mouse resize
        self.__label.setFixedSize(self.__label.width(), self.__label.height())
        # Center QLabel
        self.__label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.__label.setMovie(self.__throbber)
        self.__throbber.setScaledSize(QtCore.QSize(size, size))
        self.__throbber.start()

    def stop_throbber(self):
        """
        Stop the animation of the throbber and set the visibility of the label to Fale
        """
        self.log.debug("Stop throbber")
        self.__throbber.stop()
        self.__label.setVisible(False)

    def get_workbook_info(self):
        """
        Getter for self.__infos
        """
        return self.__infos

    def __set_workbook_info(self):
        """
        Wrapper for setting the workbook information dictionary
        """
        if isinstance(self.__wb, Book):
            self.__set_workbook_info_for_xls()
        else:
            self.__set_workbook_info_for_csv()

    def __set_workbook_info_for_xls(self):
        """
        Set the informations dictionary for xls files
        """
        self.__infos["sheets"] = self.__wb.nsheets
        self.__infos["rows"] = sum([sheet.nrows for sheet in self.__wb.sheets()])
        self.__infos["columns"] = self.__wb.sheet_by_index(0).ncols
        self.columns = [cell.value.replace("\n", " ") for cell in self.__wb.sheet_by_index(0).row(0)]

        self.log.info("Load workbook {}".format(type(self.__wb)))
        self.__set_infos_in_list()

    def __set_workbook_info_for_csv(self):
        """
        Set the informations dictionary for xls files
        """
        self.__infos["rows"] = len(self.__wb)
        self.__infos["columns"] = len(self.__wb[0])
        self.columns = self.__wb[0]

        self.log.info("Load workbook {}".format(type(self.__wb)))
        self.__set_infos_in_list()

    def __set_infos_in_list(self):
        """
        Add informations on file to listWidget
        """
        label = QLabel("sheets: {}, rows: {}, columns: {}".format(self.__infos["sheets"], self.__infos[
            "rows"], self.__infos["columns"]))
        label.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignVCenter)
        self.__layout.addWidget(label)

        button = QPushButton('')
        button.setIcon(QtGui.QIcon(os.path.join(get_assets_path(), "delete.svg")))
        button.setIconSize(QtCore.QSize(28, 28))
        button.setFixedHeight(30)
        button.setStyleSheet("QPushButton {background-color:#424242;color: #424242; height: 30px}")
        button.clicked.connect(self.__remove)
        self.__layout.addWidget(button)

    def __remove(self):
        """
        Remove self from opened_files and remove from listWidget
        """
        self.log.debug("Remove item {} from file list".format(self.name))
        self.log.debug("Now, MainWindow.opened_files={}", self.__main_window.opened_files)
        self.__main_window.remove_item_from_ui(self)

    def get_rows(self):
        if isinstance(self.__wb, Book):
            for sheet_nb in range(self.__wb.nsheets):
                for row_nb in range(1, self.__wb.sheet_by_index(sheet_nb).nrows):
                    yield self.__wb.sheet_by_index(sheet_nb).name, [cell.value for cell in self.__wb.sheet_by_index(sheet_nb).row(row_nb)]

        if isinstance(self.__wb, list):
            for i_row in range(1, len(self.__wb)):
                yield "result", self.__wb[i_row]

    def is_headers_valid(self):
        if isinstance(self.__wb, Book):
            headers = self.get_headers()
            for i_sheet in range(1, self.__wb.nsheets):
                sheet_headers =  [cell.value for cell in self.__wb.sheet_by_index(i_sheet).row(0)]
                if sheet_headers != headers:
                    return False
        return True

    def get_headers(self):
        if isinstance(self.__wb, Book):
            return [cell.value for cell in self.__wb.sheet_by_index(0).row(0)]

        if isinstance(self.__wb, list):
            return self.__wb[0]

    def get_col_index(self, column_name: str):
        for i, header in enumerate(self.get_headers()):
            if header == column_name:
                return i

    def get_total_rows(self):
        return self.__infos["rows"]
