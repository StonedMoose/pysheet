#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

from PyQt5.QtCore import pyqtSlot, QRunnable

from src.pysheet.util.Statement import Statement
from src.pysheet.util.WorkerSignals import WorkerSignals


class Query:
    __statements: [Statement]

    def __init__(self):
        super(Query, self).__init__()
        self.signals = WorkerSignals()
        self.__statements = []

    def add_statement(self, statement: Statement):
        self.__statements.append(statement)

    def pop(self):
        if self.__statements:
            self.__statements.pop()

    def clear(self):
        self.__statements = []

    def is_empty(self):
        return self.__statements == []

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < len(self.__statements):
            self.n += 1
            return self.__statements[self.n - 1]
        else:
            raise StopIteration

    def __getitem__(self, item):
        return self.__statements[item]

    def is_last_workbook2(self, wb_name):
        if self.__statements:
            return os.path.basename(self.__statements[-1].workbook2.name) == wb_name
        else:
            return False

    def get_total_steps(self):
        return self.__statements[0].file_from.get_total_rows()

    def get_headers(self):
        return self.__statements[0].file_from.get_headers()

    @pyqtSlot()
    def compute(self):
        result = {}
        wb_from = self.__statements[0].workbook1
        grouped_statements: [[Statement]] = self.build_statements()
        progress_value = 0
        last_sheet_name = None

        for sheet_name, row in wb_from.get_rows():
            if sheet_name != last_sheet_name:
                last_sheet_name = sheet_name
                result[sheet_name] = []

            predicate = None
            for group in grouped_statements:
                first_statement: Statement = group[0]
                find_result = self.find(first_statement.comparator, group[1:], row, first_statement.workbook2.get_rows(),
                                        first_statement.workbook1.get_col_index(first_statement.column1),
                                        first_statement.workbook2.get_col_index(first_statement.column2))

                if predicate is None:
                    predicate = find_result
                else:
                    if group[0].boolean_operator == "OR":
                        predicate |= find_result
                    else:
                        predicate &= find_result
            if predicate:
                result[sheet_name].append(row)
            progress_value += 1
            self.signals.progress.emit()

        self.signals.result.emit(result)

    def compare(self, comparator: str, row1: list, row2: list, i_column1: int, i_column2: int):
        """
        Compare two given rows on given column index
        :param comparator: The comparator ie. EQUAL or NOT EQUAL
        :param row1: the first row
        :param row2: the second row
        :param i_column1: the first column index
        :param i_column2: the second column index
        :return: The compare result
        """
        result_when_equal = True if comparator == "EQUAL" else False
        if row1[i_column1] == row2[i_column2]:
            return result_when_equal
        return not result_when_equal

    def find(self, comparator: str, statements: [Statement], row: list, lines: list, i_col1, i_col2):
        """

        :param statements:
        :param row:
        :return:
        """
        return_when_found = True if comparator == "IN" else False

        for sheet_name, line in lines:

            predicate = row[i_col1] == line[i_col2]

            for s in statements:
                compare_result = self.compare(s.comparator, row, line,
                                              s.workbook1.get_col_index(s.column1),
                                              s.workbook2.get_col_index(s.column2))

                if s.boolean_operator == "OR":
                    predicate |= compare_result
                elif s.boolean_operator == "AND":
                    predicate &= compare_result

            if predicate:
                return return_when_found

        return not return_when_found

    def build_statements(self):
        statements = []
        group = None
        for s in self:
            if s.comparator == "IN" or s.comparator == "NOT IN":
                if group:
                    statements.append(group)
                group = []
            group.append(s)

        statements.append(group)
        return statements
