#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
from logging import Logger

import coloredlogs
import logging as logging

from PyQt5.QtWidgets import QApplication, QDesktopWidget

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def get_logger(module_name):
    coloredlogs.install(level=os.environ['LOG_LEVEL'])
    logger: Logger = logging.getLogger(module_name)
    logger.setLevel(os.environ['LOG_LEVEL'])
    return logger

def get_alias(i: int):
    if i < 26:
        return alphabet[i]
    else:
        return alphabet[int(i / 26) - 1] + alphabet[i % 26]


class ScreenType(object):
    """
    Check if args given is a valid file path
    """
    def __init__(self, app: QApplication):
        self._app = app

    def __call__(self, value):
        if isinstance(value, str):
            n = int(value)
        else:
            n = value
        screens: [QDesktopWidget] = self._app.screens()
        if n > len(screens):
            raise argparse.ArgumentError("You have less than {} screen".format(n))

        return n - 1

def get_assets_path():
    path = os.path.join(os.path.dirname(__file__), "assets")
    if os.path.isdir(path):
        return path
    else:
        return os.path.join(os.path.dirname(__file__), "..", "ui", "assets")
