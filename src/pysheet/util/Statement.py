#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

from src.pysheet.util.Workbook import Workbook


class Statement:

    def __init__(self, workbook1: Workbook, workbook2: Workbook, column1: str, column2: str, comparator: str, file_from=None, boolean_operator=None):
        self.workbook1 = workbook1
        self.workbook2 = workbook2
        self.column1 = column1
        self.column2 = column2
        self.comparator = comparator
        self.file_from = file_from
        self.boolean_operator = boolean_operator

    def __str__(self):
        query = ""

        if self.file_from :
            query += "SELECT FROM '{}'\n".format(os.path.basename(self.workbook1.name))
            query += "WHERE "

        if self.boolean_operator:
            query += "{} ".format(self.boolean_operator)

        query += self.__get_selector()

        return query

    def __get_selector(self):
        """
        Build selector query
        """
        query = ""
        query += "'{}'.'{}' ".format(os.path.basename(self.workbook1.name), self.column1)
        if self.comparator == "EQUAL":
            query += "= "
        elif self.comparator == "NOT EQUAL":
            query += "!= "
        elif self.comparator == "IN":
            query += "IN "
        else:
            query += "NOT IN "
        query += "'{}'.'{}' ".format(os.path.basename(self.workbook2.name), self.column2)

        return query
