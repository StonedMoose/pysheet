def init_logger(level):
    coloredlogs.install(level=level)
    return logging.getLogger('some.module.name')