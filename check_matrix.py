#!/usr/bin/env python3
"""
Find rows who are in matrix xls file and not in Quickbase csv extract
"""

import xlrd
import xlwt
import csv
import os
import argparse


class FileType(object):
    """
    Check if args given is a valid file path
    """
    def __init__(self, type):
        self._type = type

    def __call__(self, value):
        if self._type == 'output':
            if os.path.isfile(value):
                value = value.replace(".xls", "(1).xls")

        elif self._type == 'input' and not os.path.isfile(value):
            raise argparse.ArgumentTypeError(
                "{} is not a valid file.".format(value))

        return value


def parse_command_line():
    """
    Parse command line arguments
    :return: the args object containing user's arguments
    """
    parser = argparse.ArgumentParser(description='Find rows which are in matrix xls file and not Quickbase csv extract')
    parser.add_argument('-m', '--matrix', type=FileType('i'),
                        help='path to matrix file',
                        required=True)
    parser.add_argument('-q', '--quickbase', type=FileType('input'),
                        help='path to Quickbase file',
                        required=True)
    parser.add_argument('-mc', '--matrix_column',
                        help='Specify column to use in matrix. default: "GIM ID"', type=str,
                        default='GIM ID')
    parser.add_argument('-qc', '--quickbase_column', type=str,
                        help='Specify column to use in quickbase. default: "Part - GIM Item ID"',
                        default='Part - GIM Item ID')
    parser.add_argument('-o', '--output_path',
                        help='Specify where to write result file',
                        default="{}/results.xls".format(os.getcwd()), type=FileType('output'))
    return parser.parse_args()


def gather_qb_references(qb_path):
    """
    Open quickbase files and build an array with every GIM ID found.
    This array will be use as a reference when looking through Matrix
    :param qb_path:
    :return: refs the references array
    """
    refs = []
    with open(os.path.abspath(qb_path), 'r') as qbFile:
        rows = csv.reader(qbFile, delimiter=";")
        for i, row in enumerate(rows):
            if i == 0:
                qb_col_index = row.index(args.quickbase_column)
            else:
                refs.append(row[qb_col_index])
    return refs


def look_in_matrix(arguments, references):
    """
    Look for matching references in matrix file
    :param arguments: the user's arguments
    :param references: the references array
    :return: a dictionary with sheets and associated matching result
    """
    print("Opening workbook...")
    with xlrd.open_workbook(arguments.matrix) as wb:
        diffs = {}
        count = 0
        total = 0
        for sheet in wb.sheets():
            sheet_total = 0
            sheet_count = 0
            print("Computing sheet {}...".format(sheet.name))
            diffs[sheet.name] = []
            diffs[sheet.name].append(sheet.row(0))
            m_col_index = [cell.value for cell in sheet.row(0)].index(arguments.matrix_column)

            for i in range(1, sheet.nrows - 1):
                sheet_total += 1
                try:
                    # If the column is empty, it may raise a ValueError
                    key = str(int(sheet.row(i)[m_col_index].value))
                    if key not in references:
                        sheet_count += 1
                        diffs[sheet.name].append(sheet.row(i))
                except ValueError:
                    diffs[sheet.name].append(sheet.row(i))

            count += sheet_count
            total += sheet_total
            print("Found {}/{} difference(s) in tab '{}'".format(sheet_count, sheet_total, sheet.name))
        print("Found {}/{} differences between '{}' and '{}'".format(count, total, arguments.matrix, arguments.quickbase))
        return diffs


def write_results(output_path, results):
    """
    Write results to xls file
    :param output_path: the path to write result to
    :param results: the result dictionary
    :return: the results
    """
    res = xlwt.Workbook()

    for sheet_name in results:
        sheet = res.add_sheet(sheet_name)
        for i_row, row in enumerate(results[sheet_name]):
            for i_col, cell in enumerate(row):
                sheet.write(i_row, i_col, cell.value)

    res.save(output_path)


if __name__ == '__main__':
    args = parse_command_line()
    refs = gather_qb_references(args.quickbase)
    differences = look_in_matrix(args, refs)
    write_results(args.output_path, differences)
